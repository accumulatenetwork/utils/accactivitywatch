FROM registry.gitlab.com/accumulatenetwork/accumulate:snapshot AS accumulate-builder


FROM mcr.microsoft.com/dotnet/sdk:7.0-jammy-amd64 AS builder

COPY . /AccActivityWatch
WORKDIR /AccActivityWatch

RUN GITHASH=$(git rev-parse HEAD)
RUN GITTAG=$(git describe --tags | sed 's/-g.*//')
RUN GITBRANCH=$(git rev-parse --abbrev-ref HEAD)
RUN GITDATE=$(git show -s --date='unix' --format=%cd HEAD)

RUN dotnet build AccActivityWatch.sln --configuration Release -p:GitHash=$(git rev-parse HEAD) -p:GitBranch=$(git rev-parse --abbrev-ref HEAD) -p:GitDateTime=$(git show -s --date='unix' --format=%cd HEAD) -p:GitTag=$(git describe --tags | sed 's/-g.*//') --output /app

FROM mcr.microsoft.com/dotnet/aspnet:7.0-jammy-amd64 AS run

COPY --from=builder /app /app
COPY --from=accumulate-builder /bin/snapshot /app

# Set the timezone.
ENV TZ=UTC
RUN ln -fs /usr/share/zoneinfo/UTC /etc/localtime

RUN apt update \
	&& apt -y install joe less ssh wget curl bash

RUN apt clean
RUN rm -rf /var/lib/apt/lists/*


WORKDIR /app
ENTRYPOINT ["/usr/bin/dotnet","AccActivityWatch.dll"]
