# AccActivityWatch

Reads Accumulate Snapshots, and populates PostgreSQL

```
docker run -it \
        --rm
        -v acc_mainnet_bvn0:/node \
        -e POSTGRES-HOST=<> \
        -e POSTGRES-PORT=5432 \
        -e POSTGRES-USER=<> \
        -e POSTGRES-PASSWORD=<> \
        -e POSTGRES-DB="Accumulate-Mainnet" \
        --name AccActivityWatch \
        registry.gitlab.com/accumulatenetwork/utils/accactivitywatch:latest [dn] <bvn0>       
```