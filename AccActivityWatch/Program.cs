﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using Npgsql;
using NpgsqlTypes;

namespace AccActivityWatch;

class Program
{
    static string? connectionString;
    private static NpgsqlConnection connection;

    static int Main(string[] args)
    {

        var POSTGRESHOST = Environment.GetEnvironmentVariable("POSTGRES-HOST");
        var POSTGRESPORT = Environment.GetEnvironmentVariable("POSTGRES-PORT") ?? "5432";

        var POSTGRESUSER = Environment.GetEnvironmentVariable("POSTGRES-USER");
        var POSTGRESPASSWORD = Environment.GetEnvironmentVariable("POSTGRES-PASSWORD");
        var POSTGRESDB = Environment.GetEnvironmentVariable("POSTGRES-DB") ?? "Accumulate-Mainnet";


        if (String.IsNullOrEmpty(POSTGRESHOST))
        {
            Console.WriteLine("POSTGRES-HOST not set");
            return 1;
        }

        if (String.IsNullOrEmpty(POSTGRESUSER) || String.IsNullOrEmpty(POSTGRESPASSWORD))
        {
            Console.WriteLine("POSTGRES-USER or POSTGRES-PASSWORD not set");
            return 1;
        }

        connectionString = $"Server={POSTGRESHOST};Port={POSTGRESPORT};Database={POSTGRESDB};User Id={POSTGRESUSER};Password={POSTGRESPASSWORD};";
        connection = new NpgsqlConnection(connectionString);
        connection.Open();

        if (args.Contains("dn")) Start("dn");
        foreach (var nodeSelect in args)
        {
            if (nodeSelect.StartsWith("bvn"))
            {
                Start(nodeSelect);
                break;
            }
        }

        connection.Dispose();

        return 0;
    }

    static void Start(string node)
    {
        Console.WriteLine("Starting snapshot list");
        using (Process p = new Process())
        {
            // Redirect the output stream of the child process.
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
#if DEBUG
            p.StartInfo.FileName = "docker";
            p.StartInfo.Arguments = "run -v acc_testnet_stable_bvn1:/node registry.gitlab.com/accumulatenetwork/accumulate:snapshot list /node/dnn/snapshots --csv";
#else
            p.StartInfo.FileName = "snapshot";
            p.StartInfo.Arguments = $"list /node/{(node == "dn" ? "dnn" : "bvnn")}/snapshots --csv";
#endif
            p.Start();
            ReadBlockCSV(new StreamReader(p.StandardOutput.BaseStream),node);
            p.WaitForExit();
        }
    }

    static void ReadBlockCSV(StreamReader reader, string network)
    {
        using (NpgsqlCommand command = new NpgsqlCommand("INSERT INTO public.\"Blocks\" (\"Height\", \"Hash\", \"TimeStamp\") VALUES (@Height, @Hash, @TimeStamp)", connection))
        {
            bool isFirstLine = true;
            string? line;
            while ((line = reader.ReadLine()) != null)
            {

                if (isFirstLine)
                {
                    isFirstLine = false;
                    continue;
                }

                string[] values = line.Split(',',StringSplitOptions.TrimEntries);

                var networkHeight = $"{network}-{values[0]}";

                if (!CheckHeightExists(networkHeight))
                {
                    var timestamp = $"{values[1].Substring(0,17)}00Z";  //2023-02-06T00:31:47Z

                    if (!string.IsNullOrEmpty(timestamp))
                    {
                        var height = values[0];
                        string hash = values[2];

                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@Height", networkHeight);
                        command.Parameters.AddWithValue("@Hash", hash);
                        command.Parameters.AddWithValue("@TimeStamp", (object)DateTime.Parse(timestamp));
                        Console.WriteLine(command.CommandText);
                        command.ExecuteNonQuery();

                        using (Process p = new Process())
                        {
                            // Redirect the output stream of the child process.
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.RedirectStandardOutput = true;
#if DEBUG
                            p.StartInfo.FileName = "docker";
                            p.StartInfo.Arguments = $"run -v acc_testnet_stable_bvn1:/node registry.gitlab.com/accumulatenetwork/accumulate:snapshot report /node/dnn/snapshots/{values[3]}";
#else
                            p.StartInfo.FileName = "snapshot";
                            p.StartInfo.Arguments = $"report /node/{(network.StartsWith("dn") ? "dnn" : "bvnn")}/snapshots/{values[3]}";
#endif
                            p.Start();
                            ReadActivityCSV(new StreamReader(p.StandardOutput.BaseStream), networkHeight);
                            p.WaitForExit();
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No blocktime for {values[0]}");
                    }

                }
                else
                {
                    Console.WriteLine($"Height exists for {values[0]}");
                }
            }
        }
    }


    static void ReadActivityCSV(StreamReader reader, string height)
    {
        var filterAccType = new string[]{"anchorLedger", "sytheticLedger","systemLedger"};

        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        {
            connection.Open();
            Console.WriteLine($"Inserting height {height}");
        //    int lastCount = 0;

            using (var writer = connection.BeginBinaryImport("COPY public.\"Activity\" (\"Height\", \"Account\", \"AccountType\", \"Chain\", \"ChainType\", \"Count\") FROM STDIN (FORMAT BINARY)"))
            {
                 string line;
                    bool isFirstLine = true;

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (isFirstLine)
                        {
                            isFirstLine = false;
                            continue;
                        }

                        string[] values = line.Split(',', StringSplitOptions.TrimEntries);
                        string account = values[0];
                        string accountType = values[1];
                        string chain = values[2];
                        string chainType = values[3];
                        var count = int.Parse(values[4]);


                        switch (chain)
                        {
                            case "main":
                            case "scratch":
                            case "signature":
                                if (!filterAccType.Any(accountType.Contains))
                                {
                                    writer.StartRow();
                                    writer.Write(height);
                                    writer.Write(account);
                                    writer.Write(accountType);
                                    writer.Write(chain);
                                    writer.Write(chainType);
                                    writer.Write(count);
                                }

                                break;
                            default:
                                continue;
                        }

                       // lastCount = count;
                    }

                    writer.Complete();
            }
        }
    }


    static bool CheckHeightExists(string height)
    {
        using (NpgsqlCommand command = new NpgsqlCommand($"SELECT COUNT(*) FROM public.\"Blocks\" WHERE \"Height\" = '{height}'", connection))
        {
            int count = Convert.ToInt32(command.ExecuteScalar());
            return (count > 0);
        }
    }


}
